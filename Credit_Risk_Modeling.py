import sys
import Csv_Fetch as cf
import Factors_Calculations as fc
import Credit_Score_Prediction as csp



def Credit_Risk_Modeling(data):
    cf.fetch_from_csv()
    print("Argument - ", data)
    credit_score = fc.Factors_Calculations_class.factors_calculation("Arpit")
    Quality_Risk_Class = csp.Credit_Score_Prediction_Class.Credit_Score_Prediction(credit_score)
    print("-------------------------")
    print(Quality_Risk_Class)


if __name__=='__main__':
    data = sys.argv[1]
    Credit_Risk_Modeling(data)