class Factors_Calculations_class:

    def factors_calculation(self):

        print("factors calculation")
        score_general = Factors_Calculations_class.Factors_General("Arpit")
        print("Score_General ", score_general)
        score_other = Factors_Calculations_class.Factors_Others("Arpit")
        print("Score_Other ", score_other)
        score_loan_details = Factors_Calculations_class.Factors_Loan_Details("Arpit")
        print("Score_loan_details ", score_loan_details)
        score_working_periods = Factors_Calculations_class.Factors_Working_Periods("Arpit")
        print("Score_Working_Periods ", score_working_periods)
        score_digital_filters = Factors_Calculations_class.Factors_Digital_Filters("Arpit")
        print("Score_Digital ", score_digital_filters)

        credit_score = score_general + score_other + score_loan_details + score_working_periods + score_digital_filters
        print("Credit Score", credit_score)

        return credit_score

    def Factors_General(self):
        print("Factors General")
        gender = "male"
        age = 20
        martial_status = "Married"
        education_level = "PhD."
        occupation = "CAT-A"
        score_factor = 0

        gender_score = 1 if gender == 'male' else 0

        age_score = 0
        if (age >= 20 and age < 30):
            age_score = 4
        elif (age >= 30 and age < 40):
            age_score = 3
        elif (age >= 40 and age < 50):
            age_score = 2
        elif (age >= 50 and age < 60):
            age_score = 1
        elif (age >= 60):
            age_score = 0

        martial_status_score = 0
        martial_status_array = ['single', 'widow', 'divorced']
        if (martial_status.lower() == 'married'):
            martial_status_score = 3
        elif (martial_status.lower() in martial_status_array):
            martial_status_score = 1

        education_level_score = 0
        if (education_level.lower() == 'phd.'):
            education_level_score = 4
        elif (education_level.lower() == 'master' or education_level.lower() == 'm-phil'):
            education_level_score = 3
        elif (education_level.lower() == 'graduate'):
            education_level_score = 2
        elif (education_level.lower() == 'inter' or education_level.lower() == 'matriculation'):
            education_level_score = 1

        else:
            education_level_score = 0

        occupation_score = 0
        if (occupation.lower() == 'supercat-a' or occupation.lower() == 'cat-a'):
            occupation_score = 3
        elif (occupation.lower() == 'cat-b'):
            occupation_score = 3
        elif (occupation.lower() == 'cat-c'):
            occupation_score = 2
        elif (occupation.lower() == 'buisnessman'):
            occupation_score = 2
        elif (occupation.lower() == 'student'):
            occupation_score = 2
        elif (occupation.lower() == 'uncategorised' or occupation.lower() == 'unemployed'):
            occupation_score = 1

        score_general = gender_score + age_score + martial_status_score + education_level_score + occupation_score
        return score_general

    def Factors_Others(self):
        print("Factors Others")
        no_of_dependents = 0
        client_locative_situation = "Own House"
        proximity = True
        monthly_net_income = 100000
        foir = 29
        credit_history = 0
        loan_from_other_bank = False
        spotmail = 100

        score_other = 0
        no_of_dependents_score = 0
        if (no_of_dependents == 0):
            no_of_dependents_score = 3
        elif (no_of_dependents == 1):
            no_of_dependents_score = 2
        elif (no_of_dependents == 2):
            no_of_dependents_score = 1
        elif (no_of_dependents >= 3):
            no_of_dependents_score = 0

        client_locative_situation_score = 0
        if (client_locative_situation.lower() == 'own house'):
            client_locative_situation_score = 3
        elif (client_locative_situation.lower() == 'parents apartment'):
            client_locative_situation_score = 2
        elif (client_locative_situation.lower() == 'rented with family'):
            client_locative_situation_score = 1
        elif (client_locative_situation.lower() >= 'rented with friends'):
            client_locative_situation_score = 0

        proximity_score = 0
        proximity_score = 2 if proximity == True else 0

        monthly_net_income_score = 0
        if (monthly_net_income >= 10000 and monthly_net_income < 25000):
            monthly_net_income_score = 0
        elif (monthly_net_income >= 25000 and monthly_net_income < 40000):
            monthly_net_income_score = 1
        elif (monthly_net_income >= 40000 and monthly_net_income < 55000):
            monthly_net_income_score = 2
        elif (monthly_net_income >= 55000 and monthly_net_income < 100000):
            monthly_net_income_score = 3
        elif (monthly_net_income >= 100000):
            monthly_net_income_score = 4

        foir_score = 0
        if (foir < 50):
            foir_score = 4
        elif (foir >= 50 and foir < 60):
            foir_score = 3
        elif (foir >= 60 and foir < 70):
            foir_score = 2
        elif (foir >= 70):
            foir_score = 1

        credit_history_score = 0
        if (credit_history == 0):
            credit_history_score = 4
        elif (credit_history > 0 and credit_history < 30):
            credit_history_score = 3
        elif (credit_history >= 30 and credit_history < 60):
            credit_history_score = 2
        elif (credit_history >= 60 and credit_history < 90):
            credit_history_score = 1

        loan_from_other_bank_score = 0
        loan_from_other_bank_score = 0 if loan_from_other_bank == True else 1

        spotmail_score = 0
        if (spotmail >= 0 and spotmail <= 100):
            spotmail_score = 4
        elif (spotmail >= 101 and spotmail < 300):
            spotmail_score = 3
        elif (spotmail >= 301 and spotmail < 600):
            spotmail_score = 2
        elif (spotmail >= 601 and spotmail < 799):
            spotmail_score = 1
        elif (spotmail >= 800):
            spotmail_score = 0

        score_other = no_of_dependents_score + client_locative_situation_score + proximity_score + monthly_net_income_score + foir_score + credit_history_score + loan_from_other_bank_score + spotmail_score
        return score_other

    def Factors_Loan_Details(self):
        print("Loan Details")
        loan_tenure = 1
        loan_period = True

        loan_tenure_score = 0
        if (loan_tenure >= 0 and loan_tenure <= 1):
            loan_tenure_score = 4
        elif (loan_tenure > 1 and loan_tenure <= 2):
            loan_tenure_score = 3
        elif (loan_tenure > 2 and loan_tenure <= 3):
            loan_tenure_score = 2
        elif (loan_tenure > 3 and loan_tenure <= 4):
            loan_tenure_score = 1
        elif (loan_tenure >= 5):
            loan_tenure_score = 0

        loan_period_score = 0
        loan_period_score = 2 if loan_period == True else 0

        score_loan_details = loan_tenure_score + loan_period_score
        return  score_loan_details

    def Factors_Working_Periods(self):
        print("Working Periods")
        current_employer = 5
        current_employer_retired = False
        last_employer = 5

        current_employer_score = 0
        if (current_employer > 0 and current_employer <= 1):
            current_employer_score = 1
        elif (current_employer_retired == True):
            current_employer_score = 1
        elif (current_employer > 1 and current_employer <= 2):
            current_employer_score = 2
        elif (current_employer > 2 and current_employer < 5):
            current_employer_score = 3
        elif (current_employer >= 5):
            current_employer_score = 4

        last_employer_score = 5
        last_employer_retired = False
        if (last_employer > 0 and last_employer <= 1):
            last_employer_score = 1
        elif (last_employer_retired == True):
            last_employer_score = 1
        elif (last_employer > 1 and last_employer <= 2):
            last_employer_score = 2
        elif (last_employer > 2 and last_employer < 5):
            last_employer_score = 3
        elif (last_employer >= 5):
            last_employer_score = 4

        score_working_periods = current_employer_score + last_employer_score
        return score_working_periods

    def Factors_Digital_Filters(self):
        print("Digital Filters")
        score_digital_factors_email = Factors_Calculations_class.Factors_Digital_Filters_Email("Arpit")
        score_digital_filters_buisness = Factors_Calculations_class.Factors_Digital_Filters_Buisness('Arpit')

        score_digital_filters = score_digital_factors_email + score_digital_filters_buisness
        return  score_digital_filters

    def Factors_Digital_Filters_Email(self):
        print("Digital_Filters_Email")
        email_to_name = True
        email_in_use = 720
        phone_to_name = True
        ip_to_address_proximity = 0
        ip_address_proxy = False

        email_to_name_score = 0
        email_to_name_score = 1 if email_to_name == True else 0

        phone_to_name_score = 0
        phone_to_name_score = 1 if phone_to_name == True else 0

        email_in_use_score = 0
        if(email_in_use >= 720):
            email_in_use_score = 2
        elif(email_in_use>=90 and email_in_use< 720):
            email_in_use_score = 1
        else:
            email_in_use_score = 0

        ip_to_address_proximity_score = 1 if(ip_to_address_proximity)<15 else 0
        if(ip_address_proxy == True):
            ip_to_address_proximity_score = 0
        score_digital_factors_email = email_to_name_score + email_to_name_score + email_in_use_score + ip_to_address_proximity_score
        return score_digital_factors_email

    def Factors_Digital_Filters_Buisness(self):
        print("Digital_Filters_Buisness")
        buisness_name_address = True
        buisness_name_address_availibility = True
        buisness_photos_available = True
        buisness_photos_raiting_20_users = True

        buisness_name_address_score = 2 if(buisness_name_address==True) else 0
        if(buisness_name_address_availibility == False):
            buisness_name_address_score = 1

        buisness_photos_score = 0
        if(buisness_photos_available == True and buisness_photos_raiting_20_users == True):
            buisness_photos_score = 2
        elif(buisness_photos_available == True and buisness_photos_raiting_20_users == False):
            buisness_photos_score = 1
        elif (buisness_photos_available == False and buisness_photos_raiting_20_users == True):
            buisness_photos_score = 2
        elif (buisness_photos_available == False and buisness_photos_raiting_20_users == False):
            buisness_photos_score = 1
        elif (buisness_photos_available == False and buisness_photos_raiting_20_users == False):
            buisness_photos_score = 0

        score_digital_filters_buisness = buisness_name_address_score + buisness_photos_score
        return  score_digital_filters_buisness
