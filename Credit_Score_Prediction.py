class Credit_Score_Prediction_Class:

    def Credit_Score_Prediction(self):
        print("Credit Score Prediction")
        credit_score = self
        credit_percent = (int(self)/63) * 100
        Quality_Risk_Class = ""

        if(credit_percent>90):
            Quality_Risk_Class = 'Highest A'
        elif(credit_percent<=90 and credit_percent >75):
            Quality_Risk_Class = 'Good B'
        elif(credit_percent<=75 and credit_percent>50):
            Quality_Risk_Class = 'Average C'
        elif(credit_percent<=50):
            Quality_Risk_Class = 'Below Average D'

        return  Quality_Risk_Class



